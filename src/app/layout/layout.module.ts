import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FooterComponent } from './components/footer/footer.component';
import {ComponentsModule} from './components/components.module';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { AddCandidateComponent } from './candidate/add-candidate/add-candidate.component';
import { AllCandidateComponent } from './candidate/all-candidate/all-candidate.component';
import {CandidateService} from './_services/candidate.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {Notification } from '../shared/notification';
import { RequirementService } from './_services/requirement.service';
import { CandidateComponent } from './candidate/candidate.component';
import { InterviewComponent } from './interview/interview.component';
import { CandidateProfileComponent } from './candidate/candidate-profile/candidate-profile.component';
import { RequirementComponent } from './requirement/requirement.component';
import { AllRequirementsComponent } from './requirement/all-requirements/all-requirements.component';
import { InterviewFeedbackComponent } from './interview/interview-feedback/interview-feedback.component';
import { DataService } from './_services/data.service';
import { ChatModule } from '@progress/kendo-angular-conversational-ui';
import { ChatService } from './_services/chat.service';
import { ChatbotComponent } from './components/chatbot/chatbot.component';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { NgbdSortableHeader } from './_services/sortable.directive';
import { StatModule } from './../shared/stat/stat.module';

@NgModule({
    imports: [
        CommonModule,
        ChartsModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule,
        ComponentsModule,
        ToastrModule.forRoot(),
        NgbModule.forRoot(),
        ReactiveFormsModule,
        FormsModule,
        ChatModule,
        DialogModule,
        StatModule
    ],
    declarations: [LayoutComponent,DashboardComponent, AddCandidateComponent, AllCandidateComponent, NgbdSortableHeader, CandidateComponent, InterviewComponent, CandidateProfileComponent, RequirementComponent, AllRequirementsComponent, InterviewFeedbackComponent,ChatbotComponent],
    providers:[CandidateService, Notification, RequirementService, DataService, ChatService],
})
export class LayoutModule {}
