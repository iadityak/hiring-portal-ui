import {CandidateFeedback} from './candidate-feedback'
export class Interview {
    candidatePanCard : String;
    interviewId: Number;
    roundNumber: Number;
    dateOfInterview: Date;
    interviewerName: String;
    domainLogic: String;
    programming: String;
    analysis: String;
    designing: String;
    integration: String;
    process: String;
    quality: String;
    testing: String;
    structuredTechniques: String;
    documentation: String;
    techSupport: String;
    sysAdministration: String;
    planningAndOrganizing: String;
    scheduleManagement: String;
    projectManagement: String;
    projectEstimationSkills: String;
    peopleManagement: String;
    overallRating: String;
    interviewerComments: String;
    roundStatus: String;
    candidateFeedback : CandidateFeedback;
}