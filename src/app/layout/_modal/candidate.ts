export class Candidate {
        firstName: String;
        lastName: String;
        email: String;
        experienceYears: String;
        onlineScore: String;
        panCard: String;
        phoneNumber: String;
        requisitionId: String;
        resume: String;
        skillSet: String;
        status: String;
        dateOfEnrollment : String;
}