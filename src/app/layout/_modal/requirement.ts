export class Requirement{
    
        dateOfOpening: Date;
        dateOfSelection: String;
        domain: String;
        jobDescription: String;
        jobStatus: String;
        position: String;
        reportingManager: String;
        requisitionId: String;
        selectedCandidate: String;
        subDomain: String
      
}