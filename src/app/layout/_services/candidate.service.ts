import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Candidate} from '../_modal/candidate';
import { Interview } from '../_modal/Interview';
import { Observable } from 'rxjs';


@Injectable()
export class CandidateService {
    baseURL: String;
    RESUME_URL: String;

    constructor(private http: HttpClient) {
        this.baseURL = environment.BASE_URL;
        this.RESUME_URL = environment.RESUME_URL;
     }

     getAllCandidate() {
        console.log("Getting All Candidate");
        return this.http.get(`${this.baseURL}/candidate/get/all`);
    }

      upload(resume: FormData){
        const url = `${this.RESUME_URL}/upload`;
        return this.http.post(url, resume);
      }

      saveResume(file: FormData,id:string){
        const url = `${this.baseURL}/resume/upload/`;
        return this.http.post(url+id, file);
      }

      addCandidate(canidate : Candidate){
        return this.http.post(`${this.baseURL}/candidate/add`, canidate);
      }

      getCandidateDetials(phoneNumber: String ){
        return this.http.get(`${this.baseURL}/candidate/get/${phoneNumber}`);
      }
      getInterviewDetails(phoneNumber : String){
        return this.http.get(`${this.baseURL}/interview/get/all/${phoneNumber}`);
      }
      getInterviewRoundDetails(interviewId:number){
        return this.http.get(`${this.baseURL}/interview/get/detail/${interviewId}`)
      }

      addInterviewRound(interview : Interview){
        return this.http.post(`${this.baseURL}/interview/add`, interview);
      }

      downloadResume(panCard : String): Observable<Blob>{
        return this.http.get(`${this.baseURL}/resume/download/${panCard}`, {  responseType: 'blob'});
      }

      getFeedbackRound(interviewId:String){
        console.log("getFeedbackRound");
       return this.http.get(`${this.baseURL}/candidate/feedback/sendemail/${interviewId}`);
      //  return this.http.get(this.baseURL+'/candidate/feedback/sendemail/');
      }

      getCandidatesCounts(){
        return this.http.get(`${this.baseURL}/dashboard/getstatuscount`);
      }
}