import { Injectable } from '@angular/core';
import { Candidate } from '../_modal/candidate';
import { Interview } from '../_modal/interview';
import { Requirement } from '../_modal/requirement';
import {BehaviorSubject} from 'rxjs/BehaviorSubject'

@Injectable()
export class DataService{
  
    
    constructor(){};
    private candidateSource = new BehaviorSubject<Candidate>(null);
    currentCandidate = this.candidateSource.asObservable();

    Interviews: Interview[];
    Requirement: Requirement;

    changeCandidate(candidate: Candidate){
        this.candidateSource.next(candidate);
    }

    // changeRoundNumber(roundNumber: number): any {
    //     this.roundSource.next(roundNumber);
    //   }

}