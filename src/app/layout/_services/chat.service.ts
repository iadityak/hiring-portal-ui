import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Action, Message, User } from '@progress/kendo-angular-conversational-ui';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChatService {  

  public readonly responses: Subject<Message> = new Subject<Message>();
  public chatUrl : string;
  private replies : string[];
  private text : string;
  private suggestedActions : string;
  private action : Action;
  private actionList : Action[] = [];
  private message : Message;


  constructor(private http: HttpClient){
    this.chatUrl=environment.CHAT_URL;    
  } 
  public readonly bot: User = {
    id: 0
  };
  

  public idemiaChat(question: string)  {

    const url  = this.chatUrl+question+"&email=";
    this.http.get(url,{'responseType': 'text'})
     .subscribe(
       result => {
        setTimeout(
          () => 
          {
            this.actionList=[];
            this.message=null;
            this.text = null;
            this.suggestedActions=null;
            if(result.indexOf("[") == -1)
            {
              this.text = result;
            }
            else
            {
              this.text = result.substring(0,result.indexOf("[")); 
              this.suggestedActions = result.substring(result.indexOf("[")+1,result.indexOf("]"));
            }
           if(this.suggestedActions!=null)
           {
            this.replies = this.suggestedActions.split(',');
            for (var i=0; i<this.replies.length; i++) {
              this.action = null;
              if(this.replies[i].startsWith("http") || this.replies[i].startsWith("www"))
              {
                this.action = {
                type : 'openUrl',
                value : this.replies[i]
                };
              }
              else
              {
                this.action = {
                type : 'reply',
                value : this.replies[i]
              };
            }
              this.actionList.push(this.action);
            }
           } 
            this.message = {
              author : this.bot,
              text : this.text,
              suggestedActions : this.actionList
            };
            this.responses.next(this.message);
          },
          1000
        );
     });
  }
}
  
