import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Candidate} from '../_modal/candidate';
import { Requirement } from '../_modal/requirement';
import { Observable } from 'rxjs';

@Injectable()
export class RequirementService {
    baseURL: String;
    RESUME_URL: String;

    constructor(private http: HttpClient) {
        this.baseURL = environment.BASE_URL;
     }

     getAllRequirements() {
        console.log("Getting All Requirements");
        return this.http.get(`${this.baseURL}/requirement/get/all`);
    }
    addRequirement(requirement : Requirement){
        return this.http.post(`${this.baseURL}/requirement/add`, requirement);
      }
     uploadAllRequirement(file : FormData){
        return this.http.post(`${this.baseURL}/requirement/upload`, file);
      }
       //fetching detail related to a particular 
       getRequirementAttribute(RquisitionId:String): Observable<any> {
        return this.http.get<any>(`${this.baseURL}/requirement/get/requirementDetail/${RquisitionId}`)
      }

      downloadSampleFormat(){
        return this.http.get(`${this.baseURL}/requirement/downloadExcelFile`);
      }

}