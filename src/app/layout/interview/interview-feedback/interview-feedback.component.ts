import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from '../../_services/data.service';
import { Candidate } from '../../_modal/candidate';
import { CandidateService } from '../../_services/candidate.service';
import { Notification } from '../../../shared/notification';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { formatDate } from '@angular/common';
import { CandidateFeedback } from '../../_modal/candidate-feedback';

@Component({
  selector: 'app-interview-feedback',
  templateUrl: './interview-feedback.component.html',
  styleUrls: ['./interview-feedback.component.scss']
})
export class InterviewFeedbackComponent implements OnInit {


  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService, private candidateService: CandidateService, private notification: Notification, private fb: FormBuilder) { }
  phoneNumber: String;
  roundNumber: String;
  candidate: Candidate;
  candFeed: CandidateFeedback;
  nextRound: Boolean;
  todaysDate = new Date();
  selectedNav: String;
  rating = ["POOR", "FAIR", "GOOD", "VERY_GOOD", "EXCELLENT"];
  roundStatus = ["ON_HOLD", "NEXT_ROUND", "SELECTED", "REJECTED"];
  rejectReason = ["Wrong Skill Set", "Bad Fit", "Not Responsive"];
  selectReason = ["Good Skill Set", "Good Fit", "Responsive"];
  interviewForm = this.fb.group({
    roundNumber: [''],
    candPhoneNumber: [''],
    dateOfInterview: [formatDate(this.todaysDate, 'yyyy-MM-dd', 'en-US', ''), Validators.required],
    interviewerName: ['', Validators.required],
    overallRating: [''],
    interviewerComments: [null],
    roundStatus: [null],
    domainLogic: [null],
    programming: [null],
    analysis: [null],
    designing: [null],
    integration: [null],
    process: [null],
    quality: [null],
    testing: [null],
    structuredTechniques: [null],
    documentation: [null],
    techSupport: [null],
    sysAdministration: [null],
    planningAndOrganizing: [null],
    scheduleManagement: [null],
    projectManagement: [null],
    projectEstimationSkills: [null],
    peopleManagement: [null],
    reason: [null]
  });


  ngOnInit() {
    this.phoneNumber = this.route.snapshot.paramMap.get("phoneNumber");
    this.roundNumber = this.route.snapshot.paramMap.get("roundNumber");
    this.dataService.currentCandidate.subscribe(can => {
      this.candidate = can;
      if (this.candidate == null) {
        this.candidateService.getCandidateDetials(this.phoneNumber).subscribe(response => {
          if (response["success"] && response["data"] != null) {
            this.candidate = response["data"];
            this.check();
          }
          else {
            this.notification.showNotification('top', 'right', `Error Ocurred!`);
          }

        },
          error => {
            this.notification.showNotification('top', 'right', `${error["error"]["message"]}`); console.log(error);
          }

        )
      }
    });

  }

  check() {
    if (this.candidate.status !== "REJECTED" && this.candidate.status !== "REJECTED") {
      this.nextRound = true;
    }
    else {
      this.nextRound = false;
    }
  }

  onSubmit() {
    this.interviewForm.patchValue({ "roundNumber": this.roundNumber });
    this.interviewForm.patchValue({ "candPhoneNumber": this.phoneNumber });
    this.candidateService.addInterviewRound(this.interviewForm.value).subscribe(Response => {
      if (Response["success"]) {
        this.notification.showNotification('top', 'right', `Interview Round Added Successfully!`);
        this.interviewForm.reset();
        this.router.navigate([`details/${this.phoneNumber}`]);
      }
      else {
        this.notification.showNotification('top', 'right', `Error Ocurred!`);
      }
    }, error => { this.notification.showNotification('top', 'right', `${error["error"]["message"]}`); })
  }
}