import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {AddCandidateComponent} from './candidate/add-candidate/add-candidate.component';
import {AllCandidateComponent} from './candidate/all-candidate/all-candidate.component';
import { CandidateComponent } from './candidate/candidate.component';
import { CandidateProfileComponent } from './candidate/candidate-profile/candidate-profile.component';
import { AllRequirementsComponent } from './requirement/all-requirements/all-requirements.component';
import { InterviewFeedbackComponent } from './interview/interview-feedback/interview-feedback.component';

const routes: Routes = [
  {
      path: '',
      component: LayoutComponent,
      children: [
          { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
          { path: 'dashboard', component: DashboardComponent },
          { path: 'add-candidate', component: AddCandidateComponent },
          { path: 'edit-candidate/:phoneNumber', component: AddCandidateComponent },
          { path: 'all-candidate', component: AllCandidateComponent },
          { path: 'candidate', component: CandidateComponent},
          { path: 'details/:phoneNumber', component: CandidateProfileComponent},
          {path: 'all-requirements', component: AllRequirementsComponent},
          {path : 'interview-feedback/:phoneNumber/:roundNumber', component : InterviewFeedbackComponent}
      ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
