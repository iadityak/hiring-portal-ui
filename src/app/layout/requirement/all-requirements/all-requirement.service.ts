import {Injectable, PipeTransform} from '@angular/core';

import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {DecimalPipe} from '@angular/common';
import {debounceTime, delay, switchMap, tap} from 'rxjs/operators';
import {SortDirection} from '../../_services/sortable.directive';
import { Notification } from '../../../shared/notification';
import { Requirement } from '../../_modal/requirement';
import { RequirementService } from '../../_services/requirement.service';

interface SearchResult {
  requirements: Requirement[];
  total: number;
}


interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(requirement: Requirement[], column: string, direction: string): Requirement[] {
  if (direction === '') {
    return requirement;
  } else {
    return [...requirement].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(requirement: Requirement, term: string, pipe: PipeTransform) {
  return requirement.requisitionId.toLowerCase().includes(term)
    || requirement.jobStatus.toLowerCase().includes(term)
    || requirement.domain.toLowerCase().includes(term)
    || requirement.position.toLowerCase().includes(term)
    || requirement.jobDescription.toLowerCase().includes(term);
}

@Injectable({providedIn: 'root'})
export class AllRequirementService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _requirements$ = new BehaviorSubject<Requirement[]>([]);
  private _total$ = new BehaviorSubject<number>(0);

  private _state: State = {
    page: 1,
    pageSize: 10,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };
    REQUIREMENTS: Requirement[];

    getAllRequirements() {
      return this.requirementService.getAllRequirements().subscribe(response => {
        console.log(response);
        if (response["success"]) {
          this.REQUIREMENTS = response["data"];
        }},
        error => {
          this.notification.showNotification('top', 'right', `Couldn't Fetch Data`);
        })
    }

  constructor(private pipe: DecimalPipe, private notification : Notification, private requirementService : RequirementService) {
    this.getAllRequirements();
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(200),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._requirements$.next(result.requirements);
      this._total$.next(result.total);
    });

    this._search$.next();
  }

  get requirements$() { return this._requirements$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: string) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let requirements = sort(this.REQUIREMENTS, sortColumn, sortDirection);

    // 2. filter
    requirements = requirements.filter(requirement => matches(requirement, searchTerm.toLowerCase(), this.pipe));
    const total = requirements.length;

    // 3. paginate
    requirements = requirements.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({requirements, total});
  }
  
}
