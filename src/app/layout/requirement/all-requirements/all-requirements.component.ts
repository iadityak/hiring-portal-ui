import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { RequirementService } from '../../_services/requirement.service';
import {Requirement} from '../../_modal/requirement';
import { Notification } from '../../../shared/notification'
import { AllRequirementService } from './all-requirement.service';
import { Observable } from 'rxjs';
import {NgbdSortableHeader, SortEvent} from './../../_services/sortable.directive';
import {DecimalPipe} from '@angular/common';
import { ExcelService } from '../../_services/excel.service';

@Component({
  selector: 'app-all-requirements',
  templateUrl: './all-requirements.component.html',
  styleUrls: ['./all-requirements.component.scss'],
  providers: [AllRequirementService, DecimalPipe]
})
export class AllRequirementsComponent implements OnInit {
  //create excel sheet for requirement
  data: any = [{
    DATE_OF_OPENING: '',
    DOMAIN: '',
    JOB_DESCRIPTION: '',
    JOB_STATUS:'',
    POSITION:'',
    REPORTING_MANAGER:'',
    REQUISITION_ID:'',
    SUB_DOMAIN:''
  },
  {
   DATE_OF_OPENING: '',
    DOMAIN: '',
    JOB_DESCRIPTION: '',
    JOB_STATUS:'',
    POSITION:'',
    REPORTING_MANAGER:'',
    REQUISITION_ID:'',
    SUB_DOMAIN:''
  },
 ];

  requirements$: Observable<Requirement[]>;
  total$: Observable<number>;
  
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;
  
  requirements: Requirement[];
  display: string;
  requirement: any;
  ispopUpShow:boolean =false;
  constructor(private requirementService: RequirementService, private notification: Notification,
     public service : AllRequirementService, private excelService:ExcelService) { 
    this.requirements$ = service.requirements$;
    this.total$ = service.total$;
  }

  ngOnInit() {
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

 
  selectedFile = null;
  onFileChanged(event) {
    console.log(event);
    this.selectedFile = event.target.files[0];
  }

  message: String = '';
  onUpload() {
    // upload code goes here
    let fd = new FormData();
    fd.append('file',this.selectedFile);
    this.requirementService.uploadAllRequirement(fd).subscribe(Response => {
      if (Response["success"]) {
        this.notification.showNotification('top', 'right', `File Uploaded Successfully!`);
      }
      else {
        this.notification.showNotification('top', 'right', `Error Ocurred!`);
      }
    }, error => { this.notification.showNotification('top', 'right', `${error["error"]["message"]}`); })

  }
  showRequirementDetail(id: String){
    this.display = 'block';
    this.ispopUpShow = true;
    this.requirementService.getRequirementAttribute(id).subscribe(response=>{
      console.log(response);
      if(response['success']) {
        this.requirement = response["data"];
      }},
      error=>{
        this.notification.showNotification('top', 'right', `Couldn't Fetch Data`)
      })
      
  }

    closePop() {
      this.ispopUpShow = false;
    }
    ClickedOut(event) {
        //debugger;
        if(event.target.className === "modal") {
          this.ispopUpShow = false;
        }
        
     }
     exportAsXLSX():void {
      this.excelService.exportAsExcelFile(this.data, 'Sample');
    }

  }

