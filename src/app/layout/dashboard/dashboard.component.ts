import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { CandidateService } from '../_services/candidate.service';
import { Notification } from '../../shared/notification'
import { Dashboard } from '../_modal/dashboard';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private candidateService: CandidateService, private notification : Notification) { 
    this.getStatusCount();
  }
  dashboard : Dashboard;

  ngOnInit() {

  }
  public pieChartLabels = ['Selected Candidates', 'Interviewed Candidates', 'Rejected Candidates'];
  //public pieChartData = [this.dashboard.selectedCount, this.dashboard.interviewedCount, this.dashboard.rejectedCount];
  public pieChartType = 'pie';

  getStatusCount(){
    this.candidateService.getCandidatesCounts().subscribe(response => {
      if (response["success"]) {
        this.dashboard = response["data"];
      }},
      error => {
        this.notification.showNotification('top', 'right', `Couldn't Fetch Data`);
      });
  }
}
