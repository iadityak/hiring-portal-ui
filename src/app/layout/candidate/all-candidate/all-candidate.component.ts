import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { CandidateService } from '../../_services/candidate.service';
import { Candidate } from '../../_modal/candidate';
import { Notification } from '../../../shared/notification'
import { AllCandidateService } from './all-candidate.service';
import {NgbdSortableHeader, SortEvent} from './../../_services/sortable.directive';
import { Observable } from 'rxjs';
import {DecimalPipe} from '@angular/common';
import { Requirement } from '../../_modal/requirement';
import { RequirementService } from '../../_services/requirement.service';
@Component({
  selector: 'app-all-candidate',
  templateUrl: './all-candidate.component.html',
  styleUrls: ['./all-candidate.component.scss'],
  providers: [AllCandidateService, DecimalPipe]
})
export class AllCandidateComponent implements OnInit {
  candidates$: Observable<Candidate[]>;
  total$: Observable<number>;
  
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;
 

  constructor(private candidateService: CandidateService, private notification: Notification,
     public service : AllCandidateService,private requirmentService:RequirementService) { 
    this.candidates$ = service.candidates$;
    this.total$ = service.total$;
  }

  //headers : String[] = ["Name", "Email", "Phone Number", "Status", "Actions"]
  candidates: Candidate[];
  ngOnInit() {
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }


  downloadResume(panCard: string) {
    const fileName = "resume_"+panCard+".pdf";
    this.candidateService.downloadResume(panCard)
    .subscribe((blob: Blob) => {
      if (navigator.msSaveBlob) 
      { 
          navigator.msSaveBlob(blob, fileName);
      }
      else 
      {
          let link = document.createElement("a");
          if (link.download !== undefined) 
          {
              let url = URL.createObjectURL(blob);
              link.setAttribute("href", url);
              link.setAttribute("download", fileName);
              link.style.visibility = 'hidden';
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
          }
      }   
  });
  }
}
