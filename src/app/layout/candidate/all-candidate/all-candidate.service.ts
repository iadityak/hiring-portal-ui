import {Injectable, PipeTransform} from '@angular/core';

import {BehaviorSubject, Observable, of, Subject} from 'rxjs';

import {Candidate} from './../../_modal/candidate';
import {DecimalPipe} from '@angular/common';
import {debounceTime, delay, switchMap, tap} from 'rxjs/operators';
import {SortDirection} from '../../_services/sortable.directive';
import { CandidateService } from '../../_services/candidate.service';
import { Notification } from '../../../shared/notification';

interface SearchResult {
  candidates: Candidate[];
  total: number;
}


interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(candidates: Candidate[], column: string, direction: string): Candidate[] {
  if (direction === '') {
    return candidates;
  } else {
    return [...candidates].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(candidate: Candidate, term: string, pipe: PipeTransform) {
  return candidate.firstName.toLowerCase().includes(term)
    || candidate.phoneNumber.includes(term)
    || candidate.status.toLowerCase().includes(term)
    || candidate.lastName.toLowerCase().includes(term)
    || candidate.email.toLowerCase().includes(term);
}

@Injectable({providedIn: 'root'})
export class AllCandidateService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _candidates$ = new BehaviorSubject<Candidate[]>([]);
  private _total$ = new BehaviorSubject<number>(0);

  private _state: State = {
    page: 1,
    pageSize: 10,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };
    CANDIDATES: Candidate[];

  getAllCanidates() {
    return this.candidateService.getAllCandidate().subscribe(response => {
      if (response["success"]) {
        this.CANDIDATES = response["data"];
        
      }},
      error => {
        this.notification.showNotification('top', 'right', `Couldn't Fetch Data`);
      })
  }

  constructor(private pipe: DecimalPipe, private candidateService : CandidateService, private notification : Notification) {
    this.getAllCanidates();
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(200),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._candidates$.next(result.candidates);
      this._total$.next(result.total);
    });

    this._search$.next();
  }

  get candidates$() { return this._candidates$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }
  set sortColumn(sortColumn: string) { this._set({sortColumn}); }
  set sortDirection(sortDirection: SortDirection) { this._set({sortDirection}); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

    // 1. sort
    let candidates = sort(this.CANDIDATES, sortColumn, sortDirection);

    // 2. filter
    candidates = candidates.filter(candidate => matches(candidate, searchTerm.toLowerCase(), this.pipe));
    const total = candidates.length;

    // 3. paginate
    candidates = candidates.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({candidates, total});
  }
}
