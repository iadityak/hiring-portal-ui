import { Component, OnInit } from '@angular/core';
import { CandidateService } from '../../_services/candidate.service';
import { ActivatedRoute } from "@angular/router";
import { Candidate } from '../../_modal/candidate';
import { Interview } from '../../_modal/interview'
import { Notification } from '../../../shared/notification';
import { checkAndUpdateBinding } from '@angular/core/src/view/util';
import { DataService } from '../../_services/data.service';
import { CandidateFeedback } from '../../_modal/candidate-feedback';

@Component({
  selector: 'app-candidate-profile',
  templateUrl: './candidate-profile.component.html',
  styleUrls: ['./candidate-profile.component.scss']
})
export class CandidateProfileComponent implements OnInit {
  ispopUpShow: boolean = false;
  interview: Interview;
  updateCand: Candidate;

  constructor(private candidateService: CandidateService, private route: ActivatedRoute, private notification: Notification, private dataService: DataService) { }
  candFeed: CandidateFeedback;
  phoneNumber: String;
  candidate: Candidate;
  viewInterview: boolean = false;
  interviews: Interview[];
  nextRound: boolean = false;
  headers = ["Round Number", "Round Name", "Round Status"];
  interviewRoundNumber = 0;
  public data;
  public show: any;
  display:String;
  public updateProfile = true;

  ngOnInit() {
    this.phoneNumber = this.route.snapshot.paramMap.get("phoneNumber");
    this.getCandidateDetails();
    this.getInterviewDetials();
  }

  getCandidateDetails() {
    this.candidateService.getCandidateDetials(this.phoneNumber).subscribe(response => {
      if (response["success"] && response["data"] != null) {
        this.candidate = response["data"];
        this.check();
      }
      else {
        this.notification.showNotification('top', 'right', `Error Ocurred!`);
      }

    },
      error => {
        this.notification.showNotification('top', 'right', `${error["error"]["message"]}`); console.log(error);
      }

    )
  }

  editCandidateProfile(){
  //   this.candidateService.getCandidateDetials(this.phoneNumber).subscribe(response => {
  //     if (response["success"] && response["data"] != null) {
  //       this.candidate = response["data"];
  //      // console.log(this.candidate);
  //   }
  // });
}

  check() {
    if (this.candidate.status !== "SELECTED" && this.candidate.status !== "REJECTED") {
      this.nextRound = true;
    }
    else {
      this.nextRound = false;
    }
  }

  getInterviewDetials() {
    this.candidateService.getInterviewDetails(this.phoneNumber).subscribe(response => {
      if (response["success"] && response["data"] != null) {
        this.interviews = response["data"];
        this.interviewRoundNumber = this.interviews.length + 1;
        if ((this.interviewRoundNumber - 1) > 0)
          this.viewInterview = true;
        else
          this.viewInterview = false;
      }
      else {
        this.notification.showNotification('top', 'right', `Error Ocurred!`);
      }

    },
      error => {
        this.notification.showNotification('top', 'right', `${error["error"]["message"]}`); console.log(error);
      }

    )
  }

  addInterviewRounds() {
    this.dataService.changeCandidate(this.candidate);
  }

  showCandidateRoundDetail(InterviewId:number) {
    this.display = 'block';
    this.ispopUpShow = true;
    this.candidateService.getInterviewRoundDetails(InterviewId).subscribe(Response=>{
      console.log(Response);
      if(Response['success']) {
        this.interview = Response['data'];
      }},
      error=>{
            this.notification.showNotification('top', 'right', `Couldn't Fetch Data`);
      }
      )
    console.log(InterviewId);

  }
  closePop() {
    this.ispopUpShow = false;
  }
  ClickedOut(event) {
      //debugger;
      if(event.target.className === "modal") {
        this.ispopUpShow = false;
      }
      
   }

  giveFeedback(InterviewId: string) {
    this.candidateService.getFeedbackRound(InterviewId).subscribe(listData => {
      this.data = listData["success"];
      if (this.data) {
        this.notification.showNotification('top', 'right', 'Feedback Email Sent Successfully!');
      }
      else {
        this.notification.showNotification('top', 'right', 'Failed to send Feedback Email!');
      }

    },
      error => {
        this.notification.showNotification('top', 'right', `${error["error"]["message"]}`);
      });
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Idemia Candidate Detail</title>
          <style>
           "../../../../../node_modules/bootstrap/dist/css/bootstrap.min.css"
          </style>
          <style>
          "../../../../assets/scss/now-ui-dashboard.scss"
         </style>
          
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
}