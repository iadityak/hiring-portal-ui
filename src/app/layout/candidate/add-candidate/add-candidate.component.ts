import { Component, OnInit } from '@angular/core';
import { CandidateService } from '../../_services/candidate.service';
import { Candidate } from '../../_modal/candidate';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { __values } from 'tslib';
import { Notification } from '../../../shared/notification';
import { RequirementService } from '../../_services/requirement.service';
import { Requirement } from '../../_modal/requirement';
import { Router, ActivatedRoute } from '@angular/router';
import { Alert } from 'selenium-webdriver';
import { Observable } from 'rxjs';
 

@Component({
  selector: 'app-add-candidate',
  templateUrl: './add-candidate.component.html',
  styleUrls: ['./add-candidate.component.scss']
})
export class AddCandidateComponent implements OnInit {
  candidate = new Candidate;
  candidateForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    experienceYears: ['', Validators.required],
    onlineScore: ['', Validators.required],
    panCard: ['', Validators.required],
    phoneNumber: ['', [Validators.required, Validators.pattern(/^[6-9]\d{9}$/)]],
    requisitionId: ['', Validators.required],
    resume: ['', Validators.required],
    skillSet: [''],
    status: ['']
  });
  phoneValue: string;
  updatedProfile: Candidate;


  constructor(private candidateService: CandidateService,
              private fb: FormBuilder,
              private notification: Notification,
              private requirementService: RequirementService,
              private router: Router,private _route:ActivatedRoute) {
  }

  requirement : Requirement[];

  ngOnInit() {
    this.requirementService.getAllRequirements().subscribe(response => {
      this.requirement = response["data"];
    },
      error => {
        this.notification.showNotification('top', 'right', `${error["error"]["message"]}`)
      });

      this._route.paramMap.subscribe(parameterMap => {
        const phone = parameterMap.get('phoneNumber');
        console.log(phone);
        this.phoneValue = phone;
        this.getCandidateDetail(phone);
      });
  }

  selectedFile = null;
  resumeUploaded : boolean = true;
  message: String = '';
  
  onSubmit() {
    if(this.selectedFile == null)
    {
      console.log("resume not selected");
      this.resumeUploaded = false;
      return;
    }
    this.candidateForm.patchValue({ "status": "PROFILE_CREATED" });
    this.candidateService.addCandidate(this.candidateForm.value).subscribe(Response => {
      if (Response["success"]) {
        let fd = new FormData();
        fd.append('file', this.selectedFile);
        if (this.selectedFile){
          this.candidateService.saveResume(fd,this.candidateForm.value.phoneNumber).subscribe(response => {
            if(response["success"]) {
              console.log("uploaded");
              this.notification.showNotification('top', 'right', `Candidate Added Successfully!`);
              this.candidateForm.reset();
              this.router.navigateByUrl("/all-candidate");
            }
          });
        } else {
          console.log("Resume Not Available");
        }
      }
      else {
        this.notification.showNotification('top', 'right', `Error Ocurred!`);
      }
    }, error => { this.notification.showNotification('top', 'right', `${error["error"]["message"]}`); })

  }

  onFileChanged(event) {
    console.log(event);
    if(event.target.files[0].size>2000000){
      alert("File is too big!");
      this.notification.showNotification('top', 'right', `max size permit 2mb!`);
    } else {
    this.selectedFile = event.target.files[0];
  //   if(this.selectedFile[0].size > 1024){
  //     console.log("size is bigger than required");
  //  };

    console.log( this.selectedFile);
  }
}

  onUpload() {
    // upload resume and Parse code goes here
    let fd = new FormData();
    fd.append('resume', this.selectedFile);
    if (this.selectedFile) {
    this.resumeUploaded = true;
      this.candidateForm.reset();
      this.candidateService.upload(fd)
        .subscribe(Response => {
          console.log(Response);
          if (Response['status'] === 200) {
            const jsonResponse = JSON.parse(JSON.stringify(Response));
            if (jsonResponse.data.basics.name.firstName) {
              this.candidateForm.patchValue({ "firstName": jsonResponse.data.basics.name.firstName });
              console.log(this.candidateForm.value["firstName"]);
            }
            if (jsonResponse.data.basics.name.lastName) {
              this.candidateForm.patchValue({ "lastName": jsonResponse.data.basics.name.lastName });
            } else {
              this.candidateForm.patchValue({ "lastName": jsonResponse.data.basics.name.surname });
            }

            if (jsonResponse.data.basics.email) {
              this.candidateForm.patchValue({ "email": jsonResponse.data.basics.email[0] });
            }
            if (jsonResponse.data.basics.phone) {
              this.candidateForm.patchValue({ "phoneNumber": jsonResponse.data.basics.phone[0] });
            }
            // temporary fix for candidate skills
            if (jsonResponse.data.skills) {
              let skillSet = '';
              jsonResponse.data.skills.forEach(skill => {
                skillSet += JSON.stringify(skill).replace('{', '').replace('}', ',');
              });
              this.candidateForm.patchValue({'skillSet': skillSet.substring(0, skillSet.length - 1)});
            }
            this.notification.showNotification('top', 'right', "Resume Parsed Successfully!");
          }
          else {
            this.notification.showNotification('top', 'right', Response['message']);
          }
        },
        error => {
          this.notification.showNotification('top', 'right', "Failed to Parse Resume!");
        });
    }
  }
  _keypress(event:any){
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
  }
  }

  private getCandidateDetail(phoneNumber:String) {
    this.candidateService.getCandidateDetials(phoneNumber).subscribe(response=>{
      this.candidate = response['data'];
      console.log(this.candidate);
    });
  }

  onUpdate(){
  }

}
