import { Component, OnInit } from '@angular/core';
import {  Message, User, SendMessageEvent } from '@progress/kendo-angular-conversational-ui';
import { Observable, Subject, merge, from } from 'rxjs';
import {  scan } from 'rxjs/operators';
import { ChatService } from '../../_services/chat.service';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})
export class ChatbotComponent { 

  public feed: Observable<Message>;
  public showChatBox:boolean = false;

  public readonly user: User = {
    id: 1
  };

  public readonly bot: User = {
    id: 0
  };

  private local: Subject<Message> = new Subject<Message>();
  
  constructor(private svc: ChatService) {

    const hello: Message = {
      author: this.bot,
      suggestedActions: [{
        type: 'reply',
        value: 'Job seeker'
      },
      {
        type: 'reply',
        value: 'Employee'
      }],
      timestamp: new Date(),
      text: 'Hello, Welcome to Idemia Chat Service ?'
    };

    this.feed = merge(
      from([ hello ]),
      this.local,
      this.svc.responses
      /*.pipe(
        map((response): Message => ({
          author: response.author,
          suggestedActions : response.suggestedActions,
          text : response.text
        })
      ))*/
    ).pipe(
      scan((acc:any, x) => acc.concat(x), [])
    );
  }

  public sendMessage(e: SendMessageEvent): void {
    this.local.next(e.message);
    this.local.next({
      author: this.bot,
      typing: true
    });
    this.svc.idemiaChat(e.message.text);
  }

  public openChatBox()
  {
    this.showChatBox = !this.showChatBox;
  }
}

