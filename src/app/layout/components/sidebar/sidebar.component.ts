import { Component, OnInit } from '@angular/core';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'design_app', class: ''},
  //  { path: '/candidate', title: 'Candidate',  icon: 'ui-1_bell-53', class: ':first-child'},
    { path: '/add-candidate', title: 'New Candidate',  icon:'users_single-02', class: '' },
    { path: '/all-candidate', title: 'All Candidates',  icon:'design_bullet-list-67', class: '' },
    // { path: '/icons', title: 'Interviews',  icon:'education_atom', class: '' },
    // { path: '/maps', title: 'Requirements',  icon:'location_map-big', class: '' },
   // { path: '/add-requirements', title: 'Add requirements',  icon:'location_map-big', class: '' },
    { path: '/all-requirements', title: 'All Requirements',  icon:'location_map-big', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ( window.innerWidth > 991) {
          return false;
      }
      return true;
  };
}
