
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class Notification {
    constructor(private toastr: ToastrService) { }

    showNotification(from, align, message) {
        this.toastr.info(`<span class="now-ui-icons ui-1_bell-53"></span> ${message}`, '', {
            timeOut: 8000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-info alert-with-icon",
            positionClass: 'toast-' + from + '-' + align
        }
        )
    }
}

