var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var RiveScript = require("rivescript");
var bot = new RiveScript();
const fs = require('fs') 

bot.loadDirectory("brain");

app.use(express.static('assets'));
var cors = require('cors')
app.use(cors())
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

app.use(allowCrossDomain);

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});
 
app.get('/getReply', function (req, res) {
  var file = req.query.email+"_lead"
  fs.writeFile(file,"User : "+req.query.msg, (err) => {if (err) throw err;})
  bot.sortReplies();
  var reply = bot.reply("local-user", req.query.msg);
  fs.writeFile(file,"AI : "+req.query.msg, (err) => {if (err) throw err;})
  res.send(reply);
});

http.listen(3000, function () {
  console.log('Started listening on *:3000');
});

